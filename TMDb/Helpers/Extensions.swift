//
//  Extensions.swift
//  TMDb
//
//  Created by Ерден on 10.10.2018.
//  Copyright © 2018 Yerden. All rights reserved.
//

import UIKit
import SVProgressHUD

extension UIAlertController {
    
    func getNetworkErrorAlert(withAction action: @escaping () -> Void) -> UIAlertController? {
        if !Connectivity.isConnectedToInternet() {
            SVProgressHUD.self.dismiss()
            
            let alert = UIAlertController(title: "Нет интернет-соединения", message: "Данные не могут быть загружены", preferredStyle: .alert)
            let confirmAction = UIAlertAction(title: "Попробуйте снова", style: .default, handler: {
                Void in
                action()
            })
            alert.addAction(confirmAction)
            
            return alert
        }
        else {
            return nil
        }
    }
    
    func getServerErrorAlert() -> UIAlertController {
        let alert = UIAlertController(title: "Ошибка сервера", message: "Попробуйте позднее", preferredStyle: .alert)
        let confirmAction = UIAlertAction(title: "Закрыть", style: .cancel, handler: nil)
        alert.addAction(confirmAction)
        return alert
    }
}


extension MoviesViewController : UISearchBarDelegate {
    
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        isSearchnig = true
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        isSearchnig = false
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        isSearchnig = false
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        isSearchnig = false
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        filteredMovies.removeAll(keepingCapacity: false)
        let predicate = searchBar.text!
        filteredMovies = movies.filter({$0.title!.range(of: predicate) != nil })
        filteredMovies.sort {$0.title! < $1.title!}
        isSearchnig = !(filteredMovies.count == 0)
        tableView.reloadData()
    }
    
}
