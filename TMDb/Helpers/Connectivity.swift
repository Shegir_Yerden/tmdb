//
//  Connectivity.swift
//  TMDb
//
//  Created by Ерден on 10.10.2018.
//  Copyright © 2018 Yerden. All rights reserved.
//

import Foundation
import AlamofireDomain

class Connectivity {
    class func isConnectedToInternet() -> Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}
