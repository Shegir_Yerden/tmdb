//
//  GenresModel.swift
//  TMDb
//
//  Created by Ерден on 10.10.2018.
//  Copyright © 2018 Yerden. All rights reserved.
//

import AlamofireDomain
import SwiftyJSON
import SVProgressHUD

class Genres: NSObject {
    
    var name: String?
    var id: String?
    
    static func fetchGenres(completionHandler: @escaping ([Genres]) -> ()) {
        
        let indicator = SVProgressHUD.self
        indicator.show()
        indicator.setDefaultMaskType(.black)
        indicator.setForegroundColor(UIColor.blue)
        
        let URL = "\(Const.URL.url)genre/movie/list?api_key=\(Const.URL.api_key)&language=ru"

        
        AlamofireDomain.request(URL, method: .get).validate().responseJSON { response in
            switch response.result {
            case .success(let value):
                
                indicator.dismiss()
                
                completionHandler(Genres.parse(value))
            case .failure:
                
                indicator.dismiss()
                
                let serverErrorAlert = UIAlertController().getServerErrorAlert()
                if let window = UIApplication.shared.keyWindow {
                    window.rootViewController?.present(serverErrorAlert, animated: true, completion: nil)
                }
                
                
            }
            
        }
    }
    
    static func parse(_ value: Any) -> [Genres] {
        var genres = [Genres]()
        let json = JSON(value)["genres"]
        for dict in json.arrayValue {
            let genre = Genres()
            genre.name = dict["name"].stringValue
            genre.id = dict["id"].stringValue
            genres.append(genre)
        }
        return genres
    }
}

