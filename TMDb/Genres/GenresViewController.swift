//
//  GenresViewController.swift
//  TMDb
//
//  Created by Ерден on 10.10.2018.
//  Copyright © 2018 Yerden. All rights reserved.
//

import UIKit

class GenresViewController: UITableViewController{
    
    let cellId = "cellId"
    
    var genres = [Genres]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "Жанры"
        tableView.register(GenresCell.self, forCellReuseIdentifier: cellId)
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.separatorStyle = .none
        checkForInternet()
        
    }
    
    func checkForInternet() {
        
        let networkErrorAlert = UIAlertController().getNetworkErrorAlert {
            self.checkForInternet()
        }
        
        if networkErrorAlert != nil {
            if let window = UIApplication.shared.keyWindow {
                window.rootViewController?.present(networkErrorAlert!, animated: true, completion: nil)
            }
        }
        else {
            Genres.fetchGenres() { (fulledGenres) in
                self.genres = fulledGenres
                self.tableView.reloadData()
            }
            
        }
        
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        if genres.count > 0 {
            return 1
        }
        return 0
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return genres.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! GenresCell
        if let title = genres[indexPath.row].name {
            cell.label.text = title
        }
        cell.selectionStyle = .none
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let moviesViewController = MoviesViewController()
        moviesViewController.type = "genre"
        if let category = genres[indexPath.row].id {
            moviesViewController.category = category
        }
        navigationController?.pushViewController(moviesViewController, animated: true)
    }
}
