//
//  GenresCell.swift
//  TMDb
//
//  Created by Ерден on 10.10.2018.
//  Copyright © 2018 Yerden. All rights reserved.
//

import UIKit
import SnapKit

class GenresCell: UITableViewCell {
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        addSubview(label)
        
        label.snp.makeConstraints { (make) in
            make.left.top.equalTo(8)
            make.right.bottom.equalTo(-8)
            make.height.equalTo(label.snp.width).multipliedBy(0.15)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let label: UILabel = {
        let label = UILabel()
        label.textColor = .blue
        label.textAlignment = .center
        return label
    }()
    
}
