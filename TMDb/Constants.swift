//
//  Constants.swift
//  TMDb
//
//  Created by Ерден on 10.10.2018.
//  Copyright © 2018 Yerden. All rights reserved.
//

import Foundation

struct Const {
    
    
    
    struct URL {
        static let imageURL = "https://image.tmdb.org/t/p/original/"
        static let url = "https://api.themoviedb.org/3/"
        static let api_key = "02da584cad2ae31b564d940582770598"
    }
    
}
