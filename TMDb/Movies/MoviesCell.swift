//
//  MoviesCell.swift
//  TMDb
//
//  Created by Ерден on 10.10.2018.
//  Copyright © 2018 Yerden. All rights reserved.
//

import UIKit
import SnapKit

class MoviesCell: UITableViewCell {
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
    let posterImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.layer.cornerRadius = 5
        return imageView
    }()
    
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = .blue
        label.font = UIFont(name: "Didot", size: 25)
        label.numberOfLines = 0
        return label
    }()
    
    
    func setupViews() {
        
        addSubview(posterImageView)
        addSubview(titleLabel)
        
        posterImageView.snp.makeConstraints { (make) in
            make.left.top.equalTo(8)
            make.width.equalTo(100)
            make.height.equalTo(posterImageView.snp.width).multipliedBy(1.5)
            make.bottom.equalTo(-8)
        }
        
        titleLabel.snp.makeConstraints { (make) in
            make.left.equalTo(posterImageView.snp.right).offset(8)
            make.centerY.equalTo(posterImageView.snp.centerY)
            make.right.equalTo(-8)
        }
        

    }
}
