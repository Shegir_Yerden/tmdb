//
//  MoviesModel.swift
//  TMDb
//
//  Created by Ерден on 10.10.2018.
//  Copyright © 2018 Yerden. All rights reserved.
//

import AlamofireDomain
import SwiftyJSON
import SVProgressHUD

class Movies: NSObject {
    
    var imageURL: String?
    var title: String?
    var id: String?
    var overview: String?
    
    static func fetchMoviesBy(_ type: String, subsection: String, completionHandler: @escaping ([Movies]) -> ()) {
        
        let indicator = SVProgressHUD.self
        indicator.show()
        indicator.setDefaultMaskType(.black)
        indicator.setForegroundColor(UIColor.blue)
        
        var URL = ""
        
        if type == "subtitle"{
            URL = "\(Const.URL.url)movie/\(subsection)?api_key=\(Const.URL.api_key)&language=ru&page=1"
        }
        else if type == "genre" {
            URL = "\(Const.URL.url)discover/movie?api_key=\(Const.URL.api_key)&language=ru&sort_by=popularity.desc&page=1&with_genres=\(subsection)"
        }
        
        AlamofireDomain.request(URL, method: .get).validate().responseJSON { response in
            switch response.result {
            case .success(let value):
                indicator.dismiss()
                completionHandler(Movies.parse(value))
            case .failure:
                
                indicator.dismiss()
                
                let serverErrorAlert = UIAlertController().getServerErrorAlert()
                if let window = UIApplication.shared.keyWindow {
                    window.rootViewController?.present(serverErrorAlert, animated: true, completion: nil)
                }
                
                
            }
            
        }
    }
    
    static func parse(_ value: Any) -> [Movies] {
        
        var movies = [Movies]()
        let json = JSON(value)["results"]
        
        for dict in json.arrayValue {
            let movie = Movies()
            movie.title = dict["title"].stringValue
            movie.id = dict["id"].stringValue
            movie.imageURL = dict["poster_path"].stringValue
            movie.overview = dict["overview"].stringValue
            movies.append(movie)
        }
        
        return movies
    }
    
    
}
