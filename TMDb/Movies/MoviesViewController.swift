//
//  MoviesViewController.swift
//  TMDb
//
//  Created by Ерден on 10.10.2018.
//  Copyright © 2018 Yerden. All rights reserved.
//

import UIKit
import Kingfisher

class MoviesViewController: UITableViewController {
    
    var type = ""
    var category = ""
    
    var isSearchBarShown = false {
        didSet {
            if isSearchBarShown {
                view.addSubview(searchBar)
            }
            else {
                searchBar.removeFromSuperview()
                isSearchnig = false
            }
        }
    }
    
    let cellId = "cellId"
    var data = [Movies]()
    var movies = [Movies]()
    var filteredMovies = [Movies]()
    var isSearchnig = false {
        didSet {
            if isSearchnig {
                data = filteredMovies
            }
            else {
                data = movies
            }
            tableView.reloadData()
        }
    }
    let searchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: Int(UIScreen.main.bounds.width), height: 40))
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(MoviesCell.self, forCellReuseIdentifier: cellId)
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.separatorStyle = .none
        navigationItem.title = "Фильмы"
        let searchButtonItem = UIBarButtonItem(title: "Поиск", style: .plain, target: self, action: #selector(changeIsSearchBarShown))
        navigationItem.rightBarButtonItems = [searchButtonItem]
        checkForInternet()
        searchBar.delegate = self
        
    }
    
    
    @objc func changeIsSearchBarShown() {
        self.isSearchBarShown = !self.isSearchBarShown
    }
    
    func checkForInternet() {
        
        let networkErrorAlert = UIAlertController().getNetworkErrorAlert {
            self.checkForInternet()
        }
        
        if networkErrorAlert != nil {
            if let window = UIApplication.shared.keyWindow {
                window.rootViewController?.present(networkErrorAlert!, animated: true, completion: nil)
            }
        }
        else {
            
            Movies.fetchMoviesBy(type, subsection: category) { (fulledMovies) in
                self.movies = fulledMovies
                self.data = fulledMovies
                self.tableView.reloadData()
            }
            
            
        }
            
    }
        
    

    override func numberOfSections(in tableView: UITableView) -> Int {
        if data.count > 0 {
            return 1
        }
        return 0
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! MoviesCell
        
        if let title = data[indexPath.row].title {
            cell.titleLabel.text = title
        }
        
        if let imageAdress = data[indexPath.row].imageURL {
            let url = URL(string: Const.URL.imageURL + imageAdress)
            cell.posterImageView.kf.setImage(with: url)
        }
        
        cell.selectionStyle = .none
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let movieDetailsViewController = MovieDetailsViewController()
        movieDetailsViewController.movieId = data[indexPath.row].id!
        navigationController?.pushViewController(movieDetailsViewController, animated: true)
    }
}
