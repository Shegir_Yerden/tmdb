//
//  ViewController.swift
//  TMDb
//
//  Created by Ерден on 10.10.2018.
//  Copyright © 2018 Yerden. All rights reserved.
//

import UIKit
import SnapKit

class TabBarController: UITabBarController{
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let subtitlesViewController = SubtitlesViewController()
        let navControlller1 = UINavigationController(rootViewController: subtitlesViewController)
        navControlller1.title = "Фильмы"
        
        
        let genresViewController = GenresViewController()
        let navControlller2 = UINavigationController(rootViewController: genresViewController)
        navControlller2.title = "Жанры"
        
        tabBar.isTranslucent = false
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedStringKey.font: UIFont(name: "Didot", size: 20)!], for: .normal)
        
        viewControllers = [navControlller1, navControlller2]
    }
    
    
    
}



class SubtitlesViewController: UIViewController{

    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "Категории"
        view.backgroundColor = .white
        
        view.addSubview(popularButton)
        view.addSubview(upcomingButton)
        
        popularButton.snp.makeConstraints { (make) in
            make.centerY.equalTo(view).offset(60)
            make.left.equalTo(8)
            make.right.equalTo(-8)
            make.height.equalTo(popularButton.snp.width).multipliedBy(0.15)
        }
        
        upcomingButton.snp.makeConstraints { (make) in
            make.centerY.equalTo(view).offset(-60)
            make.left.equalTo(8)
            make.right.equalTo(-8)
            make.height.equalTo(upcomingButton.snp.width).multipliedBy(0.15)
        }
        
        popularButton.addTarget(self, action: #selector(pushToPopularMoviesViewController), for: .touchUpInside)
        upcomingButton.addTarget(self, action: #selector(pushToUpcomingMoviesViewController), for: .touchUpInside)
        
    }
    
    
    let popularButton = createButtonWith(title: "Популярное")
    let upcomingButton = createButtonWith(title: "Скоро на экранах")
    
    
    static func createButtonWith(title: String) -> UIButton {
        let button = UIButton()
        button.setTitle(title, for: .normal)
        button.setTitleColor(.blue, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 25)
        return button
    }
    
    @objc func pushToPopularMoviesViewController() {
        let moviesViewController = MoviesViewController()
        moviesViewController.type = "subtitle"
        moviesViewController.category = "popular"
        navigationController?.pushViewController(moviesViewController, animated: true)
    }
    
    
    @objc func pushToUpcomingMoviesViewController() {
        let moviesViewController = MoviesViewController()
        moviesViewController.type = "subtitle"
        moviesViewController.category = "upcoming"
        navigationController?.pushViewController(moviesViewController, animated: true)
    }
    
    

    


}

