//
//  MovieDetailsViewController.swift
//  TMDb
//
//  Created by Ерден on 10.10.2018.
//  Copyright © 2018 Yerden. All rights reserved.
//

import UIKit
import SnapKit
import Kingfisher

class MovieDetailsViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    let cellId = "cellId"
    var movieId = ""
    var posterURL: String? {
        didSet {
            let url = URL(string: Const.URL.imageURL + posterURL!)
            imageView.kf.setImage(with: url)
        }
    }
    var overview: String? {
        didSet {
            overviewLabel.text = overview!
        }
    }
    
    var movieCast = [MovieCast]()
    var similarMovies = [SimilarMovie]()
    
    let imageView: UIImageView = {
        let iv = UIImageView()
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    let overviewLabel = createLabelWith(fontSize: 15, text: "overview")
    let castLabel = createLabelWith(fontSize: 18, text: "В ролях")
    let similarMoviesLabel = createLabelWith(fontSize: 18, text: "Похожие фильмы")
    
    let scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.backgroundColor = .white
        return scrollView
    }()
    
    lazy var castCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.showsHorizontalScrollIndicator = false
        cv.dataSource = self
        cv.delegate = self
        cv.backgroundColor = .white
        return cv
    }()
    
    lazy var similarMoviesCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.showsHorizontalScrollIndicator = false
        cv.dataSource = self
        cv.delegate = self
        cv.backgroundColor = .white
        return cv
    }()
    
    static func createLabelWith(fontSize size: CGFloat, text: String) -> UILabel {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: size)
        label.numberOfLines = 0
        label.adjustsFontForContentSizeCategory = true
        label.text = text
        label.textColor = .blue
        return label
    }
    
    func checkForInternet() {
        
        let networkErrorAlert = UIAlertController().getNetworkErrorAlert {
            self.checkForInternet()
        }
        
        if networkErrorAlert != nil {
            if let window = UIApplication.shared.keyWindow {
                window.rootViewController?.present(networkErrorAlert!, animated: true, completion: nil)
            }
        }
        else {
            
            SimilarMovie.fetchSimilarMovies(id: movieId) { (similarMovies) in
                self.similarMovies = similarMovies
                self.similarMoviesCollectionView.reloadData()
            }
            
            MovieCast.fetchMovieCast(id: movieId) { (movieCast) in
                self.movieCast = movieCast
                self.castCollectionView.reloadData()
            }
            
            MovieDetails.fetchMovieDetails(id: movieId) { (movieDetails) in
                self.posterURL = movieDetails.imageURL
                self.overview = movieDetails.overview
            }
            
            
        }
        
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if collectionView == castCollectionView {
            if movieCast.count > 0 {
                return 1
            }
            return 0
        }
        else if collectionView == similarMoviesCollectionView {
            if similarMovies.count > 0 {
                return 1
            }
            return 0
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == castCollectionView {
            return movieCast.count
        }
        else if collectionView == similarMoviesCollectionView {
            return similarMovies.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! MovieDetailsCell
        
        if collectionView == castCollectionView {
            if let imageAdress = movieCast[indexPath.row].imageURL {
                if (imageAdress != "") {
                    let url = URL(string: Const.URL.imageURL + imageAdress)
                    cell.imageView.kf.setImage(with: url)
                }
                else {
                    cell.imageView.image = #imageLiteral(resourceName: "default")
                }
            }
            cell.label.text = movieCast[indexPath.row].name
        }
        else if collectionView == similarMoviesCollectionView {
            if let imageAdress = similarMovies[indexPath.row].imageURL {
                let url = URL(string: Const.URL.imageURL + imageAdress)
                cell.imageView.kf.setImage(with: url)
            }
            cell.label.text = similarMovies[indexPath.row].name
        }
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 110, height: 200)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == similarMoviesCollectionView {
            let movieDetailsViewController = MovieDetailsViewController()
            movieDetailsViewController.movieId = similarMovies[indexPath.row].id!
            navigationController?.pushViewController(movieDetailsViewController, animated: true)
            
        }
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()

        print("movieID=", movieId)
        checkForInternet()
        
        castCollectionView.register(MovieDetailsCell.self, forCellWithReuseIdentifier: cellId)
        similarMoviesCollectionView.register(MovieDetailsCell.self, forCellWithReuseIdentifier: cellId)
        
        self.view.addSubview(scrollView)
        scrollView.snp.makeConstraints { (make) in
            make.edges.equalTo(0)
        }
    
        scrollView.addSubview(imageView)
        scrollView.addSubview(overviewLabel)
        scrollView.addSubview(castLabel)
        scrollView.addSubview(castCollectionView)
        scrollView.addSubview(similarMoviesLabel)
        scrollView.addSubview(similarMoviesCollectionView)
        
        imageView.snp.makeConstraints { (make) in
            make.centerX.equalTo(view)
            make.width.equalTo(200)
            make.top.equalTo(scrollView.snp.top)
            make.height.equalTo(imageView.snp.width).multipliedBy(1.5)
        }
        
        overviewLabel.snp.makeConstraints { (make) in
            make.top.equalTo(imageView.snp.bottom).offset(10)
            make.centerX.equalTo(view)
            make.width.equalTo(view.snp.width).offset(-16)
            make.height.equalTo(150)
        }
        
        castLabel.snp.makeConstraints { (make) in
            make.top.equalTo(overviewLabel.snp.bottom).offset(8)
            make.left.equalTo(overviewLabel.snp.left)
            make.right.equalTo(overviewLabel.snp.right)
            make.height.equalTo(50)
        }
        
        castCollectionView.snp.makeConstraints { (make) in
            make.left.equalTo(overviewLabel.snp.left)
            make.right.equalTo(overviewLabel.snp.right)
            make.top.equalTo(castLabel.snp.bottom).offset(8)
            make.height.equalTo(200)
        }
        
        similarMoviesLabel.snp.makeConstraints { (make) in
            make.top.equalTo(castCollectionView.snp.bottom).offset(8)
            make.left.equalTo(overviewLabel.snp.left)
            make.right.equalTo(overviewLabel.snp.right)
            make.height.equalTo(50)
        }
        
        similarMoviesCollectionView.snp.makeConstraints { (make) in
            make.left.equalTo(overviewLabel.snp.left)
            make.right.equalTo(overviewLabel.snp.right)
            make.top.equalTo(similarMoviesLabel.snp.bottom).offset(8)
            make.height.equalTo(castCollectionView.snp.height)
            make.bottom.equalTo(-8)
        }
        
        
    }
    
}


    

    

    
    

