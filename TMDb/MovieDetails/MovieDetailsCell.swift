//
//  MovieDetailsCell.swift
//  TMDb
//
//  Created by Ерден on 11.10.2018.
//  Copyright © 2018 Yerden. All rights reserved.
//

import UIKit
import SnapKit

class MovieDetailsCell: UICollectionViewCell {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(imageView)
        addSubview(label)
        
        imageView.snp.makeConstraints { (make) in
            make.top.left.equalTo(4)
            make.right.equalTo(-4)
            make.width.equalTo(100)
            make.height.equalTo(imageView.snp.width).multipliedBy(1.5)
        }
        
        label.snp.makeConstraints { (make) in
            make.left.equalTo(imageView.snp.left)
            make.top.equalTo(imageView.snp.bottom).offset(4)
            make.width.equalTo(imageView.snp.width)
            make.height.equalTo(40)
        }
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    let imageView = UIImageView()
    
    let label: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.font = UIFont.systemFont(ofSize: 14)
        label.textColor = .blue
        return label
    }()
}
