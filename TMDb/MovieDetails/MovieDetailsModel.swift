//
//  MovieDetailsModel.swift
//  TMDb
//
//  Created by Ерден on 11.10.2018.
//  Copyright © 2018 Yerden. All rights reserved.
//

import AlamofireDomain
import SVProgressHUD
import SwiftyJSON

class MovieDetails: NSObject {
    
    var imageURL: String?
    var overview: String?
    
    static func fetchMovieDetails(id: String, completionHandler: @escaping (MovieDetails) -> ()) {
        
        let indicator = SVProgressHUD.self
        indicator.show()
        indicator.setDefaultMaskType(.black)
        indicator.setForegroundColor(UIColor.blue)

        let URL = "\(Const.URL.url)movie/\(id)?api_key=\(Const.URL.api_key)&language=ru"
        
        
        AlamofireDomain.request(URL, method: .get).validate().responseJSON { response in
            switch response.result {
            case .success(let value):
                indicator.dismiss()
                completionHandler(MovieDetails.parse(value))
            case .failure:
                indicator.dismiss()
                let serverErrorAlert = UIAlertController().getServerErrorAlert()
                if let window = UIApplication.shared.keyWindow {
                    window.rootViewController?.present(serverErrorAlert, animated: true, completion: nil)
                }
            }
        }
    }
    
    static func parse (_ value: Any) -> MovieDetails {
        let movieDetails = MovieDetails()
        let json = JSON(value)
        movieDetails.imageURL = json["poster_path"].stringValue
        movieDetails.overview = json["overview"].stringValue
        return movieDetails
    }
    
    
    
}


class MovieCast: NSObject {
    
    var imageURL: String?
    var name: String?
    
    static func fetchMovieCast(id: String, completionHandler: @escaping ([MovieCast]) -> ()) {
        
        let indicator = SVProgressHUD.self
        indicator.show()
        indicator.setDefaultMaskType(.black)
        indicator.setForegroundColor(UIColor.blue)
        
        let URL = "\(Const.URL.url)movie/\(id)/casts?api_key=\(Const.URL.api_key)&language=ru"
        
        AlamofireDomain.request(URL, method: .get).validate().responseJSON { response in
            switch response.result {
            case .success(let value):
                indicator.dismiss()
                completionHandler(MovieCast.parse(value))
            case .failure:
                indicator.dismiss()
                let serverErrorAlert = UIAlertController().getServerErrorAlert()
                if let window = UIApplication.shared.keyWindow {
                    window.rootViewController?.present(serverErrorAlert, animated: true, completion: nil)
                }
                
                
            }
            
        }
    }
    
    static func parse(_ value: Any) -> [MovieCast] {
        var movieCasts = [MovieCast]()
        let json = JSON(value)["cast"]
        
        for dict in json.arrayValue {
            let movieCast = MovieCast()
            movieCast.imageURL = dict["profile_path"].stringValue
            movieCast.name = dict["name"].stringValue
            movieCasts.append(movieCast)
        }
        return movieCasts
    }
    
}



class SimilarMovie: NSObject {
    
    var imageURL: String?
    var name: String?
    var id: String?
    
    static func fetchSimilarMovies(id: String, completionHandler: @escaping ([SimilarMovie]) -> ()) {
        
        let indicator = SVProgressHUD.self
        indicator.show()
        indicator.setDefaultMaskType(.black)
        indicator.setForegroundColor(UIColor.blue)

        let URL = "\(Const.URL.url)movie/\(id)/similar?api_key=\(Const.URL.api_key)&language=ru&page=1"
        
        
        AlamofireDomain.request(URL, method: .get).validate().responseJSON { response in
            switch response.result {
            case .success(let value):
                
                indicator.dismiss()
                
                
                completionHandler(parse(value))
            case .failure:
                
                indicator.dismiss()
                
                let serverErrorAlert = UIAlertController().getServerErrorAlert()
                if let window = UIApplication.shared.keyWindow {
                    window.rootViewController?.present(serverErrorAlert, animated: true, completion: nil)
                }
            }
            
        }
    }
    
    static func parse(_ value: Any) -> [SimilarMovie] {
        var similarMovies = [SimilarMovie]()
        let json = JSON(value)["results"]
        
        for dict in json.arrayValue {
            let similarMovie = SimilarMovie()
            similarMovie.imageURL = dict["poster_path"].stringValue
            similarMovie.name = dict["title"].stringValue
            similarMovie.id = dict["id"].stringValue
            similarMovies.append(similarMovie)
        }
        return similarMovies
    }
}
